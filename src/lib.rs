use lv2::prelude::*;


#[derive(PortCollection)]
struct Ports {
	gain : InputPort<Control>,
	pot1 : InputPort<Control>,
	pot2 : InputPort<Control>,
	soundon : InputPort<Control>,
    output : OutputPort<Audio>
}
#[uri("https://codeberg.org/ysblokje/atari_punk_rs")]
struct PunkConsole {
	rate : f32,
	output : f32,
	run_time_astable : i32,
	run_time_monostable : i32,
}

impl Plugin for PunkConsole {
    type Ports = Ports;

    type InitFeatures = ();

    type AudioFeatures = ();

    fn new(plugin_info: &PluginInfo, features: &mut Self::InitFeatures) -> Option<Self> {
        Some(Self { rate: plugin_info.sample_rate() as f32, output: 0.0, run_time_astable: 0, run_time_monostable: 0 })
    }

    fn run(
        &mut self,
        ports: &mut Self::Ports,
        features: &mut Self::AudioFeatures,
        _sample_count: u32,
    ) {
        let high_time_astable = (0.693 * (*ports.pot1 + 1000.0) * 0.01E-6 * self.rate) as i32;
        let low_time_astable = (0.693 * (*ports.pot1) * 0.01E-6 * self.rate) as i32;
        let high_time_monostable = (0.693 * (*ports.pot2) * 0.1E-6 * self.rate) as i32;

        for out_frame in ports.output.iter_mut() {
            if self.run_time_astable >= high_time_astable + low_time_astable {
                self.run_time_astable = 0;
            }

            if self.run_time_monostable >= high_time_monostable {
                self.output = 0.0;
                if self.run_time_astable == high_time_astable {
                    self.run_time_monostable = 0;
                    self.output = 1.0;
                }
            }

            self.run_time_astable += 1;
            self.run_time_monostable += 1;
            *out_frame = if *ports.soundon > 0.5 {
                (self.output) * (*ports.gain)
            } else {
                0.0
            }
        }
    }

    fn activate(&mut self, _features: &mut Self::InitFeatures) {}

    fn deactivate(&mut self, _features: &mut Self::InitFeatures) {}

    fn extension_data(_uri: &Uri) -> Option<&'static dyn std::any::Any> {
        None
    }
}
lv2_descriptors!(PunkConsole);
